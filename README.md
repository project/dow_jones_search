# Dow Jones

## INTRODUCTION
Dow Jones module provide developer service to easily integrate 
Dow Jones APIs.

For more details visit:-
    https://developer.dowjones.com/

## REQUIREMENTS
Dow Jones username and password.

## INSTALLATION

1) Install module as usual.
https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules

2) Configure module in /admin/config/services/dow_jones.
Provide Username and Password for you dow jones account. 
Also, add some Categories codes as code:code_label

## CONFIGURATION

Dow Jones module provide developer service for different endpoints of 
Dow Jones.

Example of account Service
    Drupal::service("dow_jones.dow_jones_api")->getApNewsroomApi();

For different endpoints details visit :- https://developer.dowjones.com/site/docs/api_endpoint_reference/rest_2/index.gsp

## How to clone content in drupal

Use sub-module Dow Jones clone.
