<?php

/**
 * @file
 * Hooks related to module.
 *
 * This file contains no working PHP code; it exists to provide additional
 * documentation for doxygen as well as to document hooks in the standard
 * Drupal manner.
 */

/**
 * Alter query for dow jones search.
 *
 * @param array $variables
 *   - query :- Query format excepted by dow jones.
 *   - form :- Search form array.
 *   - form_state :- Search form state.
 */
function hook_dow_jones_search_clone_search_query_alter(array &$variables) {

}
