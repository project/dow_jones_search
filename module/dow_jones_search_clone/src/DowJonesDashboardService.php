<?php

namespace Drupal\dow_jones_search_clone;

use Drupal\dow_jones_search\DowJonesApi;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Http\ClientFactory;
use Drupal\Core\Link;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\node\Entity\Node;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class DowJonesDashboardService.
 *
 * @package Drupal\dow_jones_search_clone
 */
class DowJonesDashboardService {

  use StringTranslationTrait;

  /**
   * The Config Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Logger Factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $loggerFactory;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Request stack service object.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Content service.
   *
   * @var \Drupal\dow_jones_search\DowJonesApi
   */
  protected $dowJonesService;

  /**
   * Client factory service.
   *
   * @var \Drupal\Core\Http\ClientFactory
   */
  protected $httpClientFactory;

  /**
   * Date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * SearchAndFeedHandler constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Config factory service.
   * @param \Drupal\Core\Logger\LoggerChannelFactory $logger_factory
   *   Logger factory service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   Drupal messenger service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   Request stack service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager service.
   * @param \Drupal\dow_jones_search\DowJonesApi $dowJonesService
   *   Ap newsroom content service.
   * @param \Drupal\Core\Http\ClientFactory $httpClientFactory
   *   HTTP client factory service.
   * @param \Drupal\Core\Datetime\DateFormatter $dateFormatter
   *   Date formatter service.
   */
  public function __construct(
    ConfigFactoryInterface $configFactory,
    LoggerChannelFactory $logger_factory,
    MessengerInterface $messenger,
    RequestStack $requestStack,
    EntityTypeManagerInterface $entityTypeManager,
    DowJonesApi $dowJonesService,
    ClientFactory $httpClientFactory,
    DateFormatter $dateFormatter
  ) {
    $this->configFactory = $configFactory;
    $this->loggerFactory = $logger_factory;
    $this->messenger = $messenger;
    $this->requestStack = $requestStack;
    $this->entityTypeManager = $entityTypeManager;
    $this->dowJonesService = $dowJonesService;
    $this->httpClientFactory = $httpClientFactory;
    $this->dateFormatter = $dateFormatter;
  }

  /**
   * D.I.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   Container interface object.
   *
   * @return static
   */
  public static function create(ContainerInterface $container) {

    return new static(
      $container->get('config.factory'),
      $container->get('logger.factory'),
      $container->get('messenger'),
      $container->get('request_stack'),
      $container->get('entity_type.manager'),
      $container->get('dow_jones_search.dow_jones_api'),
      $container->get('http_client_factory'),
      $container->get('date.formatter')
    );
  }

  /**
   * Page size.
   *
   * @return string
   *   Return page size.
   */
  public function getPageSize() {
    $config = $this->configFactory->getEditable('dow_jones_search.api_config');
    return $config->get('dow_jones_search_dashboard_page_size');
  }

  /**
   * Generate table structure for listing.
   *
   * @param $decodedJsonFeedData
   *   Decode response from AP newsroom.
   *
   * @return array
   *   Return table render array.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  public function getTable($decodedJsonFeedData) {
    $header = [
      'title' => $this->t('Title'),
      'article_ref' => $this->t('Article Ref'),
      'publication_date' => $this->t('Publication Date'),
      'clone' => $this->t('Action'),
    ];

    $rows = [];
    if (isset($decodedJsonFeedData['Headlines'])) {
      $items = $decodedJsonFeedData['Headlines'];
      foreach ($items as $key => $item) {
        $clone_buttons = $this->getCloneButton($item['ArticleRef']);
        $rows[$key] = [
          'title' => [
            'data' => isset($item['Title']) ? htmlspecialchars_decode(strip_tags($this->concatenateDjProportionalArray($item['Title']))) : '',
          ],
          'article_ref' => [
            'data' => isset($item['ArticleRef']) ? $item['ArticleRef'] : '',
          ],
          'publication_date' => [
            'data' => isset($item['PublicationDate']) ?
            $this->dateFormatter->format(strtotime($item['PublicationDate'] . ' ' . $item['PublicationTime']), 'custom', 'D, m/d/Y - h:i') : '',
          ],
          'clone' => [
            'data' => !empty($clone_buttons) ? $clone_buttons : '',
          ],
        ];
      }
    }

    return [
      '#type' => 'table',
      '#id' => 'dow-jones-data',
      '#prefix' => '<div id="dow-jones-news-table-list" class="table-feed-list">',
      '#suffix' => '</div>',
      '#header' => $header,
      '#rows' => $rows,
      '#footer' => $this->getFooter($decodedJsonFeedData),
      '#empty' => $this->t('No result found.'),
    ];
  }

  /**
   * Get clone dropbutton.
   *
   * @param string $item_id
   *   Item id.
   *
   * @return string[]|null
   *   Return clone button render array.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  public function getCloneButton($item_id) {
    $mapping_config = $this->configFactory->getEditable('dow_jones_search_clone.field_mapping');
    $node_mapping = $mapping_config->get('node');
    $clone_button = ['#type' => 'dropbutton'];
    if (!empty($node_mapping)) {
      foreach ($node_mapping as $type => $node) {
        $clone_title = $this->entityTypeManager
          ->getStorage('node_type')
          ->load($type)
          ->label();
        $clone_button['#links'][$type] = [
          'title' => "Clone",
          'url' => Url::fromRoute('dow_jones_search_clone.clone_content', [
            'entity_type' => $type,
          ], [
            'query' => ['ArticleRef' => $item_id],
            'attributes' => ['target' => '_blank'],
          ]),
        ];
      }
      return $clone_button;
    }
    return NULL;
  }

  /**
   * Get footer for table.
   *
   * @param $decodedJsonFeedData
   *   Array response from Dow jones api.
   *
   * @return array
   *   Return footer for list.
   */
  public function getFooter($decodedJsonFeedData) {
    $footer = [];
    if (isset($decodedJsonFeedData['Offset']) && isset($decodedJsonFeedData['Records'])) {
      if ($decodedJsonFeedData['Offset'] > $decodedJsonFeedData['Records']) {
        $pager_param = [
          // As current offset is for next page.
          'offset' => $decodedJsonFeedData['Offset'] - 2 * $decodedJsonFeedData['Records'],
          'query' => $decodedJsonFeedData['param'],
        ];
        $footer['data'][] = $this->getPagerLinks($pager_param, '‹ Previous');
      }
    }

    if (isset($decodedJsonFeedData['Offset']) && $decodedJsonFeedData['Offset']) {
      $page_num = $decodedJsonFeedData['Offset'] / $decodedJsonFeedData['Records'];
      $footer['data'][] = $this->t('Page:') . $page_num;
    }

    if (isset($decodedJsonFeedData['Offset'])  && $decodedJsonFeedData['Offset']) {
      $pager_param = [
        'offset' => $decodedJsonFeedData['Offset'],
        'query' => $decodedJsonFeedData['param'],
      ];
      $footer['data'][] = $this->getPagerLinks($pager_param, 'Next ›');
    }
    return $footer;
  }

  /**
   * Generate pager for listing.
   *
   * @param $pager_param
   *   Pager parameters.
   * @param string $label
   *   Label of pager link.
   *
   * @return \Drupal\Core\Link
   *   Return links for pager.
   */
  public function getPagerLinks($pager_param, $label) {

    $pager_link = Link::createFromRoute($label, 'dow_jones_search_clone.pager', [
      'offset' => $pager_param['offset'],
    ], [
      'attributes' => ['class' => 'use-ajax'],
      'query' => ['query_string' => $pager_param['query']],
    ]);
    return $pager_link;
  }

  /**
   * Prepare node entity for cloning.
   *
   * @param string $entity_type
   *   Entity type to be clone.
   *
   * @return \Drupal\Core\Entity\EntityBase|\Drupal\Core\Entity\EntityInterface|null
   *   Return mapped node object.
   *
   * @throws \exception
   */
  public function prepareNodeForClone($entity_type) {

    $item_id = $this->getArticleRef();
    if ($item_id) {
      $singleContentResponse = $this->dowJonesService->getContentById($item_id);
      return $this->mapApDataToNode($entity_type, $singleContentResponse);
    }
    return NULL;
  }

  /**
   * Get ArticleRef from url.
   *
   * @return mixed
   *   Return item id from url.
   *
   * @throws \exception
   */
  public function getArticleRef() {
    $articleRef = $this->requestStack->getCurrentRequest()->query->get('ArticleRef');
    if (!$articleRef) {
      throw new \exception('Item id is not valid.');
    }
    return $articleRef;
  }

  /**
   * Map node field.
   *
   * @param string $entity_type
   *   Entity type to be clone.
   * @param array $singleContentResponse
   *   Single content response to be clone.
   *
   * @return \Drupal\Core\Entity\EntityBase|\Drupal\Core\Entity\EntityInterface
   *   Return mapped node object.
   */
  public function mapApDataToNode($entity_type, array $singleContentResponse) {
    // Prepare Node.
    $node_data = [
      'type' => $entity_type,
    ];
    $node = '';
    try {
      $node = Node::create($node_data);
    }
    catch (\Exception $exception) {
      watchdog_exception('ap_newsroom_clone', $exception);
    }

    $config = $this->configFactory->getEditable('dow_jones_search_clone.field_mapping');

    // Get all the configured entity.
    $node_list = $config->get('node');
    foreach ($node_list[$entity_type]['fields'] as $field) {
      $this->resolveFieldTypeMapping($node, $field, $singleContentResponse);
    }
    return $node;
  }

  /**
   * Field type resolver.
   *
   * @param object $entity
   *   Entity for mapping.
   * @param array $field
   *   Field details to be mapped.
   * @param array $singleContentResponse
   *   Single content response to be clone.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  public function resolveFieldTypeMapping(&$entity, array $field, array $singleContentResponse) {
    switch ($field['type']) {
      case 'dj_array':
        $this->mapDjArrayTypeField($entity, $field, $singleContentResponse);
        break;

      case 'paragraphs':
        $this->mapParagraphsType($entity, $field, $singleContentResponse);
        break;

      case 'term':
        $this->mapTermType($entity, $field, $singleContentResponse);
        break;
    }
  }

  /**
   * Map text type field.
   *
   * @param object $entity
   *   Entity for mapping.
   * @param array $field
   *   Field details to be mapped.
   * @param array $item_data
   *   Data need to map with field.
   */
  public function mapDjArrayTypeField(&$entity, array $field, array $item_data) {
    $field_name = $field['id'];
    $mapping_index = explode('.', $field['path']);
    // Loop for value.
    $field_value = $item_data['Articles'][0];
    for ($i = 0; $i < count($mapping_index); $i++) {
      if (isset($field_value[$mapping_index[$i]])) {
        $field_value = $field_value[$mapping_index[$i]];
      }
    }
    $field_value = $this->concatenateDjProportionalArray($field_value);
    if (is_string($field_value) && $entity->hasField($field_name)) {
      $entity->$field_name->appendItem($field_value);
    }
  }

  /**
   * Map paragraphs type field.
   *
   * @param object $entity
   *   Entity for mapping.
   * @param array $field
   *   Field details to be mapped.
   * @param array $item_data
   *   Data need to map with field.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  public function mapParagraphsType(&$entity, array $field, array $item_data) {
    $field_name = $field['id'];
    foreach ($field['paragraph_list'] as $para_type => $para_fields) {
      $paragraph = $this->entityTypeManager
        ->getStorage('paragraph')
        ->create(['type' => $para_type]);
      foreach ($para_fields['fields'] as $para_field) {
        $this->resolveFieldTypeMapping($paragraph, $para_field, $item_data);
      }
      if (!empty($paragraph) && $entity->hasField($field_name)) {
        $entity->$field_name->appendItem($paragraph);
      }
    }
  }

  /**
   * Map term type field.
   *
   * @param object $entity
   *   Entity for mapping.
   * @param array $field
   *   Field details to be mapped.
   * @param array $item_data
   *   Data need to map with field.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  public function mapTermType(&$entity, array $field, array $item_data) {
    $field_name = $field['id'];
    $term_name = $field['name'];
    if ($term_name) {
      $term = $this->entityTypeManager
        ->getStorage('taxonomy_term')
        ->loadByProperties(['name' => $term_name]);
      if ($term = reset($term)) {
        $entity->$field_name->appendItem($term->id());
      }
    }
  }

  /**
   * Concatenate string from paragraphs.
   *
   * @param $data_array
   *   Array to be processed.
   *
   * @return string
   */
  public function concatenateDjProportionalArray($data_array) {
    if ($data_array) {
      $full_string = '';
      foreach ($data_array as $value) {
        foreach ($value['Items'] as $item) {
          if ($item['__type'] == 'Text') {
            $full_string .= $item['Value'];
          }
          elseif ($item['__type'] == 'EntityReference') {
            $full_string .= $item['Name'];
          }
          elseif ($item['__type'] == "ELink") {
            if (!empty($item['Reference'])) {
              $link_html = '<a href="' . $item['Reference'] . '">' . $item['Text'] . '</a>';
              $full_string .= $link_html;
            }
          }
        }
      }
      // Convert "\n" to <p> tag.
      if (count(explode("\n", $full_string)) > 2) {
        $full_string = '<p>' . implode('</p><p>', array_filter(explode("\n", $full_string))) . '</p>';
      }
      return $full_string;
    }
    return '';
  }

  /**
   * Get category codes.
   *
   * @return array
   */
  public function getCategoryOption() {
    // Get all categories code from configuration.
    $config = $this->configFactory->getEditable('dow_jones_search.api_config');
    $cat_config = $config->get("dow_jones_search_dashboard_categories_code");
    $cat_array = explode("\n", $cat_config);

    // Add default index for all codes.
    $catogories[''] = $this->t('ALL');

    foreach ($cat_array as $code) {
      $catogory = explode(':', $code);
      $catogories[$catogory[0]] = $this->t($catogory[1]);
    }
    return $catogories;
  }

  /**
   * Get Category query.
   *
   * @param $category
   *   Category codes.
   *
   * @return string
   */
  public function getCategoryQuery($category) {
    if ($category == '') {
      $category_string = '';
      $count = 0;
      foreach ($this->getCategoryOption() as $code => $label) {
        if ($code) {
          if ($count == 0) {
            $category_string .= 'djn=' . $code;
          }
          else {
            $category_string .= ' OR djn=' . $code;
          }
          $count++;
        }
      }
    }
    else {
      $category_string = 'djn=' . $category;
    }
    return '(' . $category_string . ')';
  }

}
