<?php

namespace Drupal\dow_jones_search_clone\Form;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\dow_jones_search\DowJonesApi;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\dow_jones_search_clone\DowJonesDashboardService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provide a search form for search Dow jones.
 */
class DowJonesSearchForm extends FormBase {

  /**
   * DowJonesDashboardService.
   *
   * @var \Drupal\dow_jones_search_clone\DowJonesDashboardService
   */
  protected $dowJonesDashboardService;

  /**
   * DowjonesNewsroomContent.
   *
   * @var \Drupal\dow_jones_search\DowJonesApi
   */
  protected $dowJonesService;

  /**
   * Class constructor.
   *
   * @param \Drupal\dow_jones_search_clone\DowJonesDashboardService $dowJonesDashboardService
   *   DowJonesDashboardService.
   * @param \Drupal\dow_jones_search\DowJonesApi $dowJonesService
   *   DowJonesNewsroomContent.
   */
  public function __construct(DowJonesDashboardService $dowJonesDashboardService, DowJonesApi $dowJonesService) {
    $this->dowJonesDashboardService = $dowJonesDashboardService;
    $this->dowJonesService = $dowJonesService;
  }

  /**
   * Create {{ @inheritdoc }}.
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('dow_jones_search_clone.dow_jones_service'),
      $container->get('dow_jones_search.dow_jones_api')
    );
  }

  /**
   * Get form id {{ @inheritDoc }}.
   */
  public function getFormId() {
    return 'dow_jones_search_form';
  }

  /**
   * Build Form {{ @inheritDoc }}.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  public function buildForm(array $form, FormStateInterface $form_state, $decodedJsonFeedData = []) {

    $form['feedSearch'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Dow Jones Feeds Search Form'),
      '#open' => TRUE,
    ];

    $form['feedSearch']['Search'] = [
      '#title' => 'Search',
      '#type' => 'search',

    ];
    $form['feedSearch']['category'] = [
      '#type' => 'select',
      '#title' => $this->t('Category'),
      '#options' => $this->dowJonesDashboardService->getCategoryOption(),
    ];
    $form['feedSearch']['fDate'] = [
      '#title' => 'From Date',
      '#type' => 'date',
      '#attributes' => [
        'max' =>  date('Y-m-d'),
        'type' => 'date'
      ],
    ];
    $form['feedSearch']['tDate'] = [
      '#title' => 'To Date',
      '#type' => 'date',
      '#attributes' => [
        'max' =>  date('Y-m-d'),
        'type' => 'date'
      ],
    ];
    $form['feedSearch']['sort'] = [
      '#type' => 'select',
      '#title' => $this->t('Sort'),
      '#options' => [
        'Relevance' => $this->t('Relevance'),
        'DisplayDateTime' => $this->t('Display Date Time'),
        'ArrivalTime' => $this->t('Arrival Time'),
      ],
    ];
    $form['feedSearch']['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Search'),
      '#prefix' => '<div class="form-item-submit form-item">',
      '#suffix' => '</div>',
      '#ajax' => [
        'callback' => '::getDowJonesSearchResult',
        'disable-refocus' => FALSE,
        'event' => 'click',
        'wrapper' => 'dow-jones-news-table-list',
        'progress' => [
          'type' => 'throbber',
          'message' => $this->t('Getting news for you...'),
        ],
      ],
    ];

    $form['table'] = $this->dowJonesDashboardService->getTable($decodedJsonFeedData);
    $form['#dow_jones_response'] = $decodedJsonFeedData;
    return $form;
  }

  /**
   * Submit {{ @inheritDoc }}.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // @todo Needs to decide what is to be done upon form submission.
  }

  /**
   * Submit {{ @inheritDoc }}.
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    // Check for from to end date.
    $fdate = $form_state->getValue('fDate');
    $tdate = $form_state->getValue('tDate');
    $current_date = new DrupalDateTime();

    // Check for future from date.
    // Check if user added "to date" but not "from date".
    $from_date = new DrupalDateTime($fdate);
    if ((!empty($fdate) && $from_date > $current_date) || (!empty($tdate) && empty($fdate))) {
      $form_state->setError($form['feedSearch']['fDate'], $this->t("From Date can be Today’s Date or less than Today’s Date"));
      return FALSE;
    }

    // Check if "from date" is greater then "to date".
    $to_date = new DrupalDateTime($tdate);
    if ($from_date > $to_date) {
      $form_state->setError($form['feedSearch']['fDate'], $this->t("To Date must be greater then From Date"));
      return FALSE;
    }

    // If from date is added by user but not the "to date".
    // Add current date as to date.
    if (!empty($fdate) && empty($tdate)) {
      $form_state->setValueForElement($form['feedSearch']['tDate'], $current_date->format('Y-m-d'));
    }
  }

  /**
   * Get Search result from Dow Jones News.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form stage object.
   *
   * @return mixed
   *   Return search result.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \exception
   */
  public function getDowJonesSearchResult(array &$form, FormStateInterface $form_state) {

    // Get field value from form_state;.
    $query = $form_state->getValue('Search');
    $sort = $form_state->getValue('sort');
    $fdate = $form_state->getValue('fDate');
    $tdate = $form_state->getValue('tDate');
    $category = $form_state->getValue('category');
    $param = [];
    $category_query = $this->dowJonesDashboardService->getCategoryQuery($category);
    if ($query) {
      $param['SearchString'] = '(' . $query . ')' . ' AND ' . $category_query;
    }
    else {
      $param['SearchString'] = $category_query;
    }

    // Get page size.
    $page_size = $this->dowJonesDashboardService->getPageSize();
    if ($page_size) {
      $param['Records'] = (int) $page_size;
    }
    else {
      $param['Records'] = 50;
    }

    // Add start and end date.
    if ($fdate) {
      $param['StartDate'] = $fdate;
    }
    if ($tdate) {
      $param['EndDate'] = $tdate;
    }

    // Add sorting.
    $param['SortBy'] = $sort;
    $decodedJsonFeedData = $this->dowJonesService->search($param);
    $decodedJsonFeedData['param'] = $param;
    if (isset($decodedJsonFeedData['TotalRecords'])) {
      $total_record = $decodedJsonFeedData['TotalRecords'];
      $this->messenger()->addStatus($this->t("@total records found.", ["@total" => $total_record]));
    }
    $table = $this->dowJonesDashboardService->getTable($decodedJsonFeedData);
    return $table;
  }

}
