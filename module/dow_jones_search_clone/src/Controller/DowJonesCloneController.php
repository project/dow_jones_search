<?php

namespace Drupal\dow_jones_search_clone\Controller;

use Drupal\dow_jones_search_clone\DowJonesDashboardService;
use Drupal\dow_jones_search_clone\Entity\DowJonesCloneEntityFormBuilder;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\node\Controller\NodeController;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Returns responses for Quick Node Clone Node routes.
 */
class DowJonesCloneController extends NodeController {

  /**
   * The entity form builder.
   *
   * @var \Drupal\dow_jones_search_clone\Entity\DowJonesCloneEntityFormBuilder
   */
  protected $dowJonesCloneEntityFormBuilder;

  /**
   * XML feed content.
   *
   * @var \Drupal\dow_jones_search_clone\DowJonesDashboardService
   */
  protected $dowJonesDashboardService;

  /**
   * Logger Factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $loggerFactory;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Exception message.
   *
   * @var string
   */
  protected $exceptionMessage = 'Something went wrong please content site admin for more details.';

  /**
   * Constructs a NodeController object.
   *
   * @param \Drupal\dow_jones_search_clone\Entity\DowJonesCloneEntityFormBuilder $entity_form_builder
   *   The entity form builder.
   * @param \Drupal\dow_jones_search_clone\DowJonesDashboardService $dowJonesDashboardService
   *   Dashboard service.
   * @param \Drupal\Core\Logger\LoggerChannelFactory $logger_factory
   *   Passing an Instance of logger factory.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   */
  public function __construct(
    DowJonesCloneEntityFormBuilder $entity_form_builder,
    DowJonesDashboardService $dowJonesDashboardService,
    LoggerChannelFactory $logger_factory,
    MessengerInterface $messenger
  ) {
    $this->dowJonesCloneEntityFormBuilder = $entity_form_builder;
    $this->dowJonesDashboardService = $dowJonesDashboardService;
    $this->loggerFactory = $logger_factory;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('dow_jones_search_clone.entity.form_builder'),
      $container->get('dow_jones_search_clone.dow_jones_service'),
      $container->get('logger.factory'),
      $container->get('messenger')
    );
  }

  /**
   * Return form to clone feed into News.
   *
   * @param string $entity_type
   *   Entity type to be cloned.
   *
   * @return array
   *   Return form render array.
   *
   * @throws \exception
   */
  public function cloneContent($entity_type) {

    $node = $this->dowJonesDashboardService->prepareNodeForClone($entity_type);
    if (!empty($node)) {
      $form = $this->entityFormBuilder()->getForm($node);
      return $form;
    }
    else {
      throw new NotFoundHttpException();
    }
  }

  /**
   * Retrieves the entity form builder.
   *
   * @return \Drupal\dow_jones_search_clone\Entity\DowJonesCloneEntityFormBuilder
   *   The entity form builder.
   */
  protected function entityFormBuilder() {
    return $this->dowJonesCloneEntityFormBuilder;
  }

}
