<?php

namespace Drupal\dow_jones_search_clone\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\dow_jones_search\DowJonesApi;
use Drupal\dow_jones_search_clone\DowJonesDashboardService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Url;

/**
 * Class DowJonesDashboardController.
 *
 * @package Drupal\dow_jones_search_clone\Controller
 */
class DowJonesDashboardController extends ControllerBase {

  /**
   * Variable used for loading Dow jones clone service.
   *
   * @var \Drupal\dow_jones_search_clone\DowJonesDashboardService
   */
  protected $dowJonesCloneService;

  /**
   * Dow jones content service.
   *
   * @var \Drupal\dow_jones_search\DowJonesApi
   */
  protected $dowJonesService;

  /**
   * Constructs a NodeController object.
   *
   * @param \Drupal\Core\Logger\LoggerChannelFactory $logger_factory
   *   Passing an Instance of logger factory.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\dow_jones_search_clone\DowJonesDashboardService $dowJonesCloneService
   *   Ap dashboard service.
   * @param \Drupal\dow_jones_search\DowJonesApi $dowJonesService
   *   Ap newsroom service.
   */
  public function __construct(
    LoggerChannelFactory $logger_factory,
    MessengerInterface $messenger,
    DowJonesDashboardService $dowJonesCloneService,
    DowJonesApi $dowJonesService
  ) {
    $this->loggerFactory = $logger_factory;
    $this->messenger = $messenger;
    $this->dowJonesCloneService = $dowJonesCloneService;
    $this->dowJonesService = $dowJonesService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('logger.factory'),
      $container->get('messenger'),
      $container->get('dow_jones_search_clone.dow_jones_service'),
      $container->get('dow_jones_search.dow_jones_api')
    );
  }

  /**
   * Lists Feeds.
   *
   * @return array
   *   Feed list render array.
   *
   * @throws \exception
   */
  public function listFeeds() {
    if (!empty($this->dowJonesService->getDowJonesUsername() && $this->dowJonesService->getDowjonesPassword() )) {
      $param = [];
      // Default query for search.
      $query = $this->dowJonesCloneService->getCategoryQuery('');
      $param['SearchString'] = $query;
      // Get page size.
      $page_size = $this->dowJonesCloneService->getPageSize();
      if ($page_size) {
        $param['Records'] = (int) $page_size;
      } else {
        $param['Records'] = 50;
      }
      $decodedJsonFeedData = $this->dowJonesService->search($param);
      $decodedJsonFeedData['param'] = $param;
      $form = $this->formBuilder()
      ->getForm('Drupal\dow_jones_search_clone\Form\DowJonesSearchForm', $decodedJsonFeedData);
      // Add message.
      $prefix = $form['table']['#prefix'];
      if (isset($decodedJsonFeedData["TotalRecords"])) {
        $record_count = $decodedJsonFeedData["TotalRecords"];
      } else {
        $record_count = 0;
      }
      $msg_string = '<div class="messages messages--status">About ' . $record_count . ' result found.</div>';
      $form['table']['#prefix'] = $prefix . $msg_string;
      return $form;
    }
    else {
      $dowJonesConfigLink = Url::fromRoute('dow_jones_search.api_config')->toString();
      $message = $this->messenger->addWarning($this->t('Dow Jones Username and
      Password is not set, for connecting with down jones API add
      dow jones username and password in the config
       <a href="@url"> Add Dow Jones Config </a>',
      ['@url' => $dowJonesConfigLink]));
      return [
        '#markup$' => $message,
      ];
    }

  }

  /**
   * Method for updating data upon pagination.
   *
   * @param $offset
   *   Offset of page.
   *
   * @param $query
   *   Query to be search.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   Return Ajax command
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  public function updateData($offset) {

    $param = \Drupal::request()->query->get('query_string');
    $param['Offset'] = $offset;
    $decodedJsonFeedData = $this->dowJonesService->search($param);
    $decodedJsonFeedData['param'] = $param;
    $table = $this->dowJonesCloneService->getTable($decodedJsonFeedData);
    $response = new AjaxResponse();
    $response->addCommand(new ReplaceCommand('#dow-jones-news-table-list', $table));
    return $response;
  }

}
