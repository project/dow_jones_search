# Dow Jones clone

## INTRODUCTION
Dow Jones Clone module provide dashboard and clone functionality for 
Dow Jones content.

## REQUIREMENTS
Dow Jones Username and password.

## INSTALLATION

1) Install module as usual.
https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules

2) Configure module in /admin/config/services/dow_jones.
   Provide Username and Password for you dow jones account. 
   Also, add some Categories codes as code:code_label

## CONFIGURATION
See example_config/dow_jones_clone.field_mapping.yml

Add your content mapping accordingly and import this configuration.

