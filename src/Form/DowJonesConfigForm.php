<?php

namespace Drupal\dow_jones_search\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ApFeedConfForm.
 */
class DowJonesConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'dow_jones_search.api_config',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'dow_jones_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('dow_jones_search.api_config');

    $form['dow_jones_config'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Dow Jones API configurations'),
    ];

    $form['dow_jones_config']['username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Username'),
      '#default_value' => $config->get('username'),
      '#required' => TRUE,
      '#description' => $this->t('Username of Dow jones.'),
    ];
    $form['dow_jones_config']['password'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Password'),
      '#default_value' => $config->get('password'),
      '#description' => $this->t('Dow Jones password.'),
    ];
    $form['dow_jones_config']['namespace'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Namespace'),
      '#default_value' => $config->get('namespace'),
      '#description' => $this->t('Dow Jones API namespace.'),
    ];
    $form['dow_jones_config']['api_version'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Version'),
      '#default_value' => $config->get('api_version'),
      '#description' => $this->t('Dow Jones api version.'),
    ];
    $form['dow_jones_config']['token_cache'] = [
      '#type' => 'checkbox',
      '#title' => t('Enable caching access token.'),
      '#default_value' => $config->get('token_cache'),
      '#description' => t('Check if you want to enable access token caching.'),
    ];
    $form['dow_jones_config']['cache_time'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Cache Time'),
      '#default_value' => $config->get('cache_time'),
      '#description' => $this->t('Time in sec for which token should be store in cache.'),
    ];
    return parent::buildForm($form, $form_state);

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $this->config('dow_jones_search.api_config')
      ->set('username', $form_state->getValue('username'))
      ->set('password', $form_state->getValue('password'))
      ->set('namespace', $form_state->getValue('namespace'))
      ->set('api_version', $form_state->getValue('api_version'))
      ->set('token_cache', $form_state->getValue('token_cache'))
      ->set('cache_time', $form_state->getValue('cache_time'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
