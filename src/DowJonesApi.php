<?php

namespace Drupal\dow_jones_search;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\Core\Utility\Error;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class DowJonesApi.
 */
class DowJonesApi {

  use StringTranslationTrait;

  /**
   * Base URL for Dow Jones.
   */
  const DOW_JONES_URL = "https://api.dowjones.com/api/Public/";

  /**
   * The Config Factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Logger Factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $loggerFactory;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * API url not correct.
   *
   * @var string
   */
  protected $urlNotCorrectError = "URL you trying is not correct.
    Please verify URL or contact site admin for more details.";

  /**
   * Exception message for editor.
   *
   * @var string
   */
  protected $exceptionMsg = "Something has been wrong with Dow Jones.
    Please contact Site Admin.";

  /**
   * Client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * Default cache bin service.
   *
   * @var
   */
  protected $cache;

  /**
   * SearchAndFeedHandler constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Config factory service.
   * @param \Drupal\Core\Logger\LoggerChannelFactory $logger_factory
   *   Logger factory service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   Messenger Service.
   * @param \GuzzleHttp\Client $httpClient
   *   Http client service.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   */
  public function __construct(
    ConfigFactoryInterface $configFactory,
    LoggerChannelFactory $logger_factory,
    MessengerInterface $messenger,
    Client $httpClient,
    CacheBackendInterface $cache
  ) {
    $this->configFactory = $configFactory;
    $this->loggerFactory = $logger_factory;
    $this->messenger = $messenger;
    $this->httpClient = $httpClient;
    $this->cache = $cache;
  }

  /**
   * D.I.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   Container.
   *
   * @return static
   */
  public static function create(ContainerInterface $container) {

    return new static(
      $container->get('config.factory'),
      $container->get('logger.factory'),
      $container->get('messenger'),
      $container->get('http_client'),
      $container->get('cache.default')
    );
  }

  /**
   * Send request to URL.
   *
   * @param string $url
   *   Api full url.
   *
   * @return bool|string
   *   Return response from Ap newsroom.
   */
  public function sendRequest($url) {
    if (empty($url)) {
      $this->messenger->addError($this->t("Error while sending the request: @error", ["@error" => $this->urlNotCorrectError]));
      return FALSE;
    }

    try {
      $response = $this->httpClient->get($url);
      $data = $response->getBody();
      if (empty($data)) {
        return FALSE;
      }
      return $data->getContents();
    }
    catch (RequestException $e) {
      $exception_message = $this->t("@exception", ["@exception" => $this->exceptionMsg]);
      $this->messenger->addError($this->t("@exception", [
        "@code" => $e->getCode(),
        "@exception" => $exception_message->__toString(),
      ]));
      $variables = Error::decodeException($e);
      $this->loggerFactory->get('dow_jones')
        ->error('%type: @message in %function (line %line of %file).', $variables);
      return FALSE;
    }
  }

  /**
   * Get username.
   *
   * @return array|mixed
   *   Return username.
   *
   * @throws \exception
   */
  public function getUsername() {
    $config = $this->configFactory->getEditable('dow_jones_search.api_config');
    $username = $config->get('username');
    if (!$username) {
      throw new \exception("username can't be empty.");
    }
    return $username;
  }

  /**
   * Get password.
   *
   * @return array|mixed
   *   Return password.
   *
   * @throws \exception
   */
  public function getPassword() {
    $config = $this->configFactory->getEditable('dow_jones_search.api_config');
    $password = $config->get('password');
    if (!$password) {
      throw new \exception("password can't be empty.");
    }
    return $password;
  }

  /**
   * Get namespace.
   *
   * @return array|mixed
   *   Return namespace.
   *
   * @throws \exception
   */
  public function getNamespace() {
    $config = $this->configFactory->getEditable('dow_jones_search.api_config');
    $namespace = $config->get('namespace');
    if (!$namespace) {
      throw new \exception("namespace can't be empty.");
    }
    return $namespace;
  }

  /**
   * Get version to be used for API.
   *
   * @return array|mixed|string
   *   Return API version.
   */
  public function getApiVersion() {
    $config = $this->configFactory->getEditable('dow_jones_search.api_config');
    $ver = $config->get('api_version');
    if ($ver) {
      return $ver;
    }
    else {
      return '2.0';
    }
  }

  /**
   * Get Cache time to be used for token.
   *
   * @return array|mixed|string
   *   Return time in secs.
   */
  public function getCacheTime() {
    $config = $this->configFactory->getEditable('dow_jones_search.api_config');
    $secs = $config->get('cache_time');
    if ($secs) {
      return $secs;
    }
    else {
      // Default return 2 days.
      return 172800;
    }
  }

  /**
   * Check if token cache enable.
   *
   * @return array|mixed|null
   */
  public function isTokenCacheEnabled() {
    $config = $this->configFactory->getEditable('dow_jones_search.api_config');
    return $config->get('token_cache');
  }

  /**
   * Get feed from dow jones.
   *
   * @param array $param
   *   Check Dow Jones developer portal for parameters list.
   *
   * @return array|bool
   *   Return feed from Dow jones.
   *
   * @throws \exception
   */
  public function search($param = []) {
    $param['SearchMode'] = 'Traditional';
    return $this->getContent('Content/Headlines/json', $param);
  }

  /**
   * Generate API url.
   *
   * @param string $api_endpoint
   *   - Visit Dow jones developer portal for endpoints.
   * @param array $options
   *   Parameters to be use in api call.
   *
   * @return string
   *   Return API Url.
   *
   * @throws \exception
   */
  public function generateApiUrl($api_endpoint, array $options = []) {
    $uri = $this->getBaseUrl() . $api_endpoint;
    $url = Url::fromUri($uri, $options);
    return $url->toUriString();
  }

  /**
   * Get base URL with version.
   *
   * @return string
   *   Return base url.
   */
  public function getBaseUrl() {
    $base_url = self::DOW_JONES_URL;
    $api_ver = $this->getApiVersion();
    if ($api_ver) {
      $base_url .= $api_ver . "/";
    }
    return $base_url;
  }

  /**
   * Get Encrypted token.
   *
   * @return false|mixed
   *
   * @throws \exception
   */
  public function getEncryptedToken() {

    // Check for cache.
    if ($this->isTokenCacheEnabled()) {
      $encrypted_token = $this->cache->get('dow_jones:encrypted_token');
      if($encrypted_token) {
        return $encrypted_token->data;
      }
    }

    $options['query'] = [
      'userid' => $this->getUsername(),
      'password' => $this->getPassword(),
      'namespace' => $this->getNamespace(),
      'parts' => 'EncryptedToken',
    ];

    $token_api = $this->generateApiUrl("Session/Login/json", $options);
    $response = $this->sendRequest($token_api);
    if ($response) {
      $response = json_decode($response);
      if($this->isTokenCacheEnabled()) {
        // Set Cache for faster retrieval.
        $this->setTokenCache($response);
      }
      return $response;
    }
    else {
      $this->messenger->addError($this->t("Something went wrong while retrieving access token."));
      $this->loggerFactory->get('dow_jones_search')
        ->error('Something went wrong while retrieving access token. Response %response', [
          '%response' => $response,
        ]);
    }
    return FALSE;
  }

  /**
   * Set token cache.
   *
   * @param $token
   */
  public function setTokenCache($token) {
    $cid = "dow_jones:encrypted_token";
    $valid_time = \Drupal::time()->getRequestTime() + 172800;
    $this->cache->set($cid, $token, $valid_time);
  }

  /**
   * Get content from Dow Jones.
   *
   * @param $api_endpoint
   *   API end point.
   * @param array $param
   *   Parameter array to use in api.
   *
   * @return array|bool
   *   Return response from Dow Jones.
   *
   * @throws \exception
   */
  public function getContent($api_endpoint, array $param) {
    if ($encrypted_token = $this->getEncryptedToken()) {
      $token = $encrypted_token->EncryptedToken;
      $param['EncryptedToken'] = $token;
      $options = [
        'query' => $param,
      ];
      $url = $this->generateApiUrl($api_endpoint, $options);
      return Json::decode($this->sendRequest($url));
    }
    else {
      return FALSE;
    }
  }

  /**
   * Fetch the contentitem object for a single piece of content by its Item ID.
   *
   * @param string $articleRef
   *   Item id.
   * @param array $param
   *   Check dow jones developer portal for parameters list.
   *
   * @return array
   *   Return content by id.
   *
   * @throws \exception
   */
  public function getContentById($articleRef, array $param = []) {
    if (!$articleRef) {
      throw new exception('Content id cannot be null.');
    }
    $param['articleRef'] = $articleRef;
    return $this->getContent('Content/article/articleRef/Json', $param);
  }

  /**
   * Get Confif Immutable username.
   *
   * @return array|mixed
   *   Return username.
   *
   * @throws \exception
   */
  public function getDowJonesUsername() {
    $config = $this->configFactory->get('dow_jones_search.api_config');
    $username = $config->get('username');
    return $username;
  }

  /**
   * Get Confif Immutable Password.
   *
   * @return array|mixed
   *   Return Password.
   *
   * @throws \exception
   */
  public function getDowjonesPassword() {
    $config = $this->configFactory->get('dow_jones_search.api_config');
    $password = $config->get('password');
    return $password;
  }

}
